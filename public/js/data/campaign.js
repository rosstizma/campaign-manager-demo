var CampaignManager = CampaignManager || {};

CampaignManager.campaign = function(params) {

  var campaign = this;
  this.name = params.name || "";
  this.id = params.id;
  this.outletData = [];
  this.outlets = params.outlets || [];
  this.overallBudget = params.overallBudget || 0;
  this.totals = {};
  this.groups = [];
  this.dayperiodTotals = {};
  this.budget = params.budget || 0;


  var initialize = function() {
    if ( campaign.id != null ) {
      campaign.generateStatsForCampaign();
    }
  }

  this.generateStatsForCampaign = function() {
    var daysBack = 365;
    var date = new Date();
    var dayPeriod = Math.floor(date.getTime() / (1000*60*60*24));
    date.setDate(date.getDate()-daysBack);
    var outletMap = {};
    var outletData = {};
    for ( var i = 0; i < campaign.outlets.length; i++ ) {
      outletData[campaign.outlets[i].id] = {};
      outletMap[campaign.outlets[i].id] = campaign.outlets[i];
    }
    var dayperiodTotals = {};
    var totals = {
      views: 0
    };
    var cdp = Math.floor(date.getTime() / (1000*60*60*24));
    for ( var i = cdp-30; i <= dayPeriod; i++ ) {
      dayperiodTotals[i] = {
        views: 0
      };
      for ( var key in outletData ) {
        outletData[key][i] = {
          name: key,
          views: Math.floor(Math.random()*1000)
        }
        totals.views += outletData[key][i].views;
        dayperiodTotals[i].views += outletData[key][i].views;
      }
    }
    campaign.outletData = outletData;
    campaign.totals = totals;
    campaign.dayperiodTotals = dayperiodTotals;
  }

  initialize();

}
