'use strict';

/* App Module */

var campaignApp = angular.module('campaignApp', [
  'ngRoute',
  'campaignControllers'
]);

phonecatApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/campaigns', {
        templateUrl: 'campaigns/campaign-list.html',
        controller: 'CampaignListCtrl'
      }).
      when('/campaigns/:campaignId', {
        templateUrl: 'campaigns/campaign-item.html',
        controller: 'CampaignItemCtrl'
      }).
      otherwise({
        redirectTo: '/campaigns'
      });
  }]);
