var CampaignManager = CampaignManager || {};

CampaignManager.phoneManager = function(params) {

  var initialize = function() {
    jQuery.ajax({
      url: "data/phones.json",
      type: "GET",
      dataType: "json",
      success: function(data) {
          jQuery("#phoneViewList").empty();
          for ( var i = 0; i < data.length; i++ ) {
            jQuery("#phoneViewList").append(generatePhoneItem(data[i]));
          }
      }
    });
  }

  var generatePhoneItem = function(item) {
    var tmpl = jQuery("#phoneViewListTemplates").find(".phoneItem")[0].cloneNode(true);
    jQuery(tmpl).find(".phoneName").val(item.name);
    jQuery(tmpl).find(".phoneItemNumber").val(item.number);
    for ( var i = 0; i < item.options.length; i++ ) {
      jQuery(tmpl).find(".phoneItemOptions").append(generateOptionItem(item.options[i]));
    }
    return tmpl;
  }

  var generateOptionItem = function(option) {
    var tmpl = jQuery("#phoneViewListTemplates").find(".phoneOption")[0].cloneNode(true);
    jQuery(tmpl).find(".optionDescription").val(option.description);
    jQuery(tmpl).find(".optionNumber").val(option.number);
    jQuery(tmpl).find(".keypadOption").val(option.keypadOption);
    return tmpl;
  }

  initialize();

}
