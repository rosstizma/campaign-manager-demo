'use strict';

/* Controllers */

var campaignControllers = angular.module('campaignControllers', []);

campaignControllers.controller('CampaignListCtrl', ['$scope', 'Campaign',
  function($scope, Campaign) {
    $scope.phones = Campaign.query();
  }]);

campaignControllers.controller('CampaignItemCtrl', ['$scope', '$routeParams', 'Campaign',
  function($scope, $routeParams, Phone) {
    $scope.phone = Campaign.get({campaignId: $routeParams.campaignId}, function(campaign) {

    });
  }]);
