var campaigns = ["Medstar Georgetown Pancreatic",
"Medstar Georgetown Liver","Medstar Georgetown Kidney","Medstar Georgetown Epilepsy",
"Medstar Georgetown Movement Disorder"];
var sites = ["Google","Bing"];

var date = new Date();
var currentCamps = [];
var currentDayperiod = Math.floor((new Date().getTime()/(1000*60*60*24)));

function generateStatsForCampaign(d) {
  var daysBack = 365;
  var date = new Date();
  var dayPeriod = Math.floor(date.getTime() / (1000*60*60*24));
  date.setDate(date.getDate()-daysBack);
  var outlets = {};
  for ( var i = 0; i < d.outlets.length; i++ ) {
    outlets[d.outlets[i].id] = {};
  }
  var dayperiodTotals = {};
  var totals = {
    views: 0
  };
  var cdp = Math.floor(date.getTime() / (1000*60*60*24));
  for ( var i = cdp-30; i <= dayPeriod; i++ ) {
    dayperiodTotals[i] = {
      views: 0
    };
    for ( var key in outlets ) {
      outlets[key][i] = {
        name: key,
        views: Math.floor(Math.random()*1000)
      }
      totals.views += outlets[key][i].views;
      dayperiodTotals[i].views += outlets[key][i].views;
    }
  }
  return {
    outlets: outlets,
    totals: totals,
    dayperiodTotals: dayperiodTotals
  };
}

// #overview
// #campaign/id
function checkHash() {
  jQuery.ajax({
    url: "data/campaign.json",
    type: "GET",
    dataType: "json",
    success: function(d) {
      selectedCamp = new CampaignManager.campaign(d);
    }
  })
}

var campaignData = [];

function getData() {
  jQuery.ajax({
    url: "data/overview.json",
    success: function(d) {
      campaignData = [];
      for ( var i = 0; i < d.campaigns.length; i++ ) {
        campaignData.push(new CampaignManager.campaign(d.campaigns[i]));
      }
      changeDay();
    }
  })
}

function changeDay() {
  drawCharts();
  showCampaigns();
  jQuery("#campaignList").hide();
  jQuery("#campaignsLi").removeClass("active");
  jQuery("#overviewLi").addClass("active");
  jQuery("#overviewContainer").show();
}

var chartTop5 = null;
var chartPhoneVsWeb = null;
var mostCostEfficientCampaigns = null;
function drawCharts() {
  //overall web vs phone
  var dayperiodWeb = 0;
  var dayperiodPhone = 0;
  for ( var i = 0; i < campaignData.length; i++ ) {
    for ( var x = 0; x < campaignData[i].outlets.length; x++ ) {
      var outlet = campaignData[i].outlets[x];
      if ( outlet.type == "phone" ) {
        dayperiodWeb += campaignData[i].outletData[outlet.id][currentDayperiod].views;
      } else {
        dayperiodPhone += campaignData[i].outletData[outlet.id][currentDayperiod].views;
      }
    }
  }
  if ( chartPhoneVsWeb == null ) {
    jQuery("#chartPhoneVsWeb").empty();
    var canvas = document.createElement("canvas");
    jQuery(canvas).attr({
      "data-chart":"doughnut",
      "width":"150px",
      "height":"150px"
    }).addClass("ex-graph").appendTo("#chartPhoneVsWeb");
    chartPhoneVsWeb = new Chart(canvas.getContext("2d")).Doughnut([
      {
        value: dayperiodWeb,
        label: "Website Views",
        color: "#1ca8dd"
      },
      {
        value: dayperiodPhone,
        label: "Phone Calls",
        color: "#1bc98e"
      }],
      {
        animation: true,
        animationSteps: 20,
        animateScale: true,
        animationEasing: "easeOutQuart",
        segmentStrokeColor: "#252830",
        segmentStrokeWidth: 2,
        tooltipTemplate: "<%= value %>",
        percentageInnerCutout: 80
      }
    );
  } else {
    // chartPhoneVsWeb.segments[0].value = dayperiodWeb;
    // chartPhoneVsWeb.segments[1].value = dayperiodPhone;
    chartPhoneVsWeb.addData(
      {
        value: dayperiodWeb,
        label: "Website Views",
        color: "#1ca8dd"
      });
    chartPhoneVsWeb.addData(
      {
        value: dayperiodPhone,
        label: "Phone Calls",
        color: "#1bc98e"
    });
    chartPhoneVsWeb.removeData(0);
    chartPhoneVsWeb.removeData(0);
  }

  jQuery("#phoneVsWebLegend").html(chartPhoneVsWeb.generateLegend());


  //top 5 campaigns for the day
  campaignData.sort(function(a,b) {
    if ( a.dayperiodTotals[currentDayperiod].views > b.dayperiodTotals[currentDayperiod].views )
      return -1;
    return 1;
  })

  jQuery("#chartTop5Campaigns").empty();
  var canvas = document.createElement("canvas");
  jQuery(canvas).attr({
    "data-chart":"doughnut",
    "width":"150px",
    "height":"150px"
  }).addClass("ex-graph").appendTo("#chartTop5Campaigns");
  var data = [];
  var colors = ["#1ca8dd","#1bc98e","#e64759","#e4d836","#9f86ff"];
  for ( var i = 0; i < campaignData.length && i < 5; i++ ) {
    data.push({
      value: campaignData[i].dayperiodTotals[currentDayperiod].views,
      label: campaignData[i].name,
      color: colors[i]
    })
  }
  var c = new Chart(canvas.getContext("2d")).Doughnut(data,
    {
      animation: true,
      animationSteps: 20,
      animationEasing: "linear",
      segmentStrokeColor: "#252830",
      segmentStrokeWidth: 2,
      tooltipTemplate: "<%= value %>",
      percentageInnerCutout: 80
    }
  );
  jQuery("#top5CampaignsLegend").html(c.generateLegend());

  //top campaign breakdown
  jQuery("#customChart").empty();
  var canvas = document.createElement("canvas");
  jQuery(canvas).attr({
    "data-chart":"bar",
    "width":"150px",
    "height":"150px"
  }).addClass("ex-graph").appendTo("#customChart");
  var data = [];
  var colors = ["#1ca8dd","#1bc98e","#e64759","#e4d836","#9f86ff"];
  var topCamp = campaignData[0];
  var campMap = {};
  var labels = [];
  var dat = [];
  campaignData.sort(function(a,b) {
    var one = a.budget/a.totals.views;
    var two = b.budget/a.totals.views;
    if ( one > two )
      return 1;
    return -1;
  })
  var data = [];
  for ( var i = 0; i < campaignData.length && i < 5; i++ ) {
    data.push({
      value: Math.floor(100*campaignData[i].budget/(campaignData[i].totals.views/1000))/100,
      label: campaignData[i].name,
      color: colors[i]
    })
  }
  var c = new Chart(canvas.getContext("2d")).Doughnut(data,
    {
      animation: true,
      animationSteps: 20,
      animationEasing: "linear",
      tooltipTemplate: "$<%= value %>",
      segmentStrokeColor: "#252830",
      segmentStrokeWidth: 2,
      percentageInnerCutout: 80,
    }
  );
  jQuery("#customChartLegend").html(c.generateLegend());

  var last5DaysPhone = {};
  var last5DaysWeb = {};
  for ( var x = currentDayperiod-5; x < currentDayperiod; x++ ) {
    last5DaysPhone[x] = 0;
    last5DaysWeb[x] = 0;
  }
    for ( var i = 0; i < campaignData.length; i++ ) {
      for ( var x = 0; x < campaignData[i].outlets.length; x++ ) {
        var outlet = campaignData[i].outlets[x];
        if ( outlet.type == "phone" ) {
          for ( var n = currentDayperiod-5; n < currentDayperiod; n++ ) {
              last5DaysPhone[n] += campaignData[i].outletData[outlet.id][n].views;
          }
        } else {
          for ( var n = currentDayperiod-5; n < currentDayperiod; n++ ) {
              last5DaysWeb[n] += campaignData[i].outletData[outlet.id][n].views;
          }
        }
      }
    }

    var last5DaysPhoneDat = [];
    for ( var key in last5DaysPhone ) {
        last5DaysPhoneDat.push(last5DaysPhone[key]);
    }
    jQuery("#last5DaysPhoneChart").empty();
    var canvas = document.createElement("canvas");
    jQuery(canvas).attr({
      "data-chart":"spark-line",
      "width":"200px",
      "height":"45px"
    }).addClass("ex-graph").appendTo("#last5DaysPhoneChart");
    var c = new Chart(canvas.getContext("2d")).Bar({
      labels: labels,
      datasets: [
        {
            label: "",
            fillColor: 'rgba(255,255,255,.3)',
            strokeColor: '#fff',
            pointStrokeColor: '#fff',
            data: last5DaysPhoneDat
        }
      ]
    },
    {
      animation: false,
      responsive: true,
      bezierCurve : true,
      bezierCurveTension : 0.25,
      showScale: false,
      pointDotRadius: 0,
      pointDotStrokeWidth: 0,
      pointDot: false,
      showTooltips: false
    });

    var last5DaysWebDat = [];
    for ( var key in last5DaysWeb ) {
        last5DaysWebDat.push(last5DaysWeb[key]);
    }
    jQuery("#last5DaysChart").empty();
    var canvas = document.createElement("canvas");
    jQuery(canvas).attr({
      "data-chart":"spark-line",
      "width":"200px",
      "height":"45px"
    }).addClass("ex-graph").appendTo("#last5DaysChart");
    var c = new Chart(canvas.getContext("2d")).Bar({
      labels: labels,
      datasets: [
        {
            label: "",
            fillColor: 'rgba(255,255,255,.3)',
            strokeColor: '#fff',
            pointStrokeColor: '#fff',
            data: last5DaysWebDat
        }
      ]
    },
    {
      animation: false,
      responsive: true,
      bezierCurve : true,
      bezierCurveTension : 0.25,
      showScale: false,
      pointDotRadius: 0,
      pointDotStrokeWidth: 0,
      pointDot: false,
      showTooltips: false
    });

}

function showTelephoneManager() {
    jQuery("#telephoneLi").addClass("active");
    jQuery("#telephoneContainer").show();
    jQuery("#individualCampaign").hide();
    jQuery("#campaignsLi").removeClass("active");
    jQuery("#overviewLi").removeClass("active");
    jQuery("#campaignList").hide();
    jQuery("#overviewContainer").hide();
    jQuery("#campaignViewList").hide();
    jQuery("#campaignCreator").hide();
    var telephoneManager = new CampaignManager.phoneManager({});
}

function showOverview() {
  jQuery("#telephoneLi").removeClass("active");
  jQuery("#telephoneContainer").hide();
  jQuery("#campaignsLi").removeClass("active");
  jQuery("#overviewLi").addClass("active");
  jQuery("#campaignList").hide();
  jQuery("#overviewContainer").show();
  jQuery("#campaignViewList").show();
  jQuery("#individualCampaign").hide();
  jQuery("#campaignCreator").hide();
}

function newCampaign() {
  var camp = new CampaignManager.campaign({});
  var campManager = new CampaignManager.campaignEditor({
    campaign: camp
  });
  campaignData.push(camp);
  jQuery("#campaignViewList").hide();
  jQuery("#campaignCreator").show();
}

function createCampaign() {
  jQuery("#campaignViewList").show();
  jQuery("#campaignCreator").hide();
}

function showCampaigns() {
    jQuery("#telephoneLi").removeClass("active");
    jQuery("#telephoneContainer").hide();
  jQuery("#individualCampaign").hide();
  jQuery("#campaignsLi").addClass("active");
  jQuery("#overviewLi").removeClass("active");
  jQuery("#campaignList").show();
  jQuery("#overviewContainer").hide();
  jQuery("#campaignViewList").show();
  jQuery("#campaignCreator").hide();
  var templ = '<a class="list-group-item" href="#">      <span class="pull-right text-muted"></span>      $name   </a>';
  jQuery("#campaignViewList").empty();
  for ( var i = 0; i < campaignData.length; i++ ) {
    var div = document.createElement('span');
    div.innerHTML = templ.replace("$name",campaignData[i].name);
    addClickToDiv(div, campaignData[i]);
    jQuery("#campaignViewList").append(div);
  }
}

function addClickToDiv(div, campaignData) {
  jQuery(div).click(function() {
    showCampaign(campaignData);
  })
}

function showCampaign(campaignData) {
  jQuery("#campaignsLi").removeClass("active");
  jQuery("#campaignsLi").removeClass("active");
  jQuery("#campaignList").hide();
  jQuery("#overviewContainer").hide();
  jQuery("#individualCampaign").show();
  //#allCampTraffic
  jQuery("#allCampTraffic").empty();
  var canvas = document.createElement("canvas");
  jQuery(canvas).attr({
    "data-chart":"bar",
    "width":"250px",
    "height":"500px"
  }).addClass("ex-graph").appendTo("#allCampTraffic");
  jQuery("#campaignTitle").text(campaignData.name);
  jQuery("#campaignDatePicker").unbind("change").change(function() {
    var d = new Date(jQuery("#campaignDatePicker").val());
    currentDayperiod = Math.floor(d.getTime() / (1000*60*60*24));
    showCampaign(campaignData);
  })
  var data = [];
  var colors = ["#1ca8dd","#1bc98e","#e64759","#e4d836","#9f86ff"];
  var campMap = {};
  var labels = [];
  var cpm = [];
  var dat = [];
  for ( var i = 0; i < campaignData.outlets.length; i++ ) {
    labels.push(campaignData.outlets[i].name);
    dat.push(campaignData.outletData[campaignData.outlets[i].id][currentDayperiod].views);
    cpm.push(Math.floor(100*campaignData.outlets[i].budget/campaignData.outletData[campaignData.outlets[i].id][currentDayperiod].views/1000)/100);
  } //Math.floor(100*campaignData[i].budget/(campaignData[i].totals.views/1000))/100
  var c = new Chart(canvas.getContext("2d")).Bar({
    labels: labels,
    datasets: [
      {
          label: "Top Outlets",
          fillColor: "#1ca8dd",
          strokeColor: "rgba(220,220,220,0.8)",
          highlightFill: colors[1],
          highlightStroke: "rgba(220,220,220,1)",
          data: dat
      }
    ]
  },
    {
      animation: true,
      animationSteps: 20,
      animationEasing: "linear",
      segmentStrokeColor: "#252830",
      segmentStrokeWidth: 1,
      percentageInnerCutout: 80
    }
  );

  jQuery("#campCPM").empty();
  var canvas = document.createElement("canvas");
  jQuery(canvas).attr({
    "data-chart":"bar",
    "width":"250px",
    "height":"500px"
  }).addClass("ex-graph").appendTo("#campCPM");

  var c = new Chart(canvas.getContext("2d")).Bar({
    labels: labels,
    datasets: [
      {
          label: "Cost Per 1000 Views",
          fillColor: "#1ca8dd",
          strokeColor: "rgba(220,220,220,0.8)",
          highlightFill: colors[2],
          highlightStroke: "rgba(220,220,220,1)",
          data: cpm
      }
    ]
  },
    {
      animation: true,
      animationSteps: 20,
      animationEasing: "linear",
      segmentStrokeColor: "#252830",
      segmentStrokeWidth: 1,
      percentageInnerCutout: 80
    }
  );

  for ( var i = 0; i < campaignData.length && i < 5; i++ ) {
    data.push({
      value: Math.floor(100*campaignData[i].budget/(campaignData[i].totals.views/1000))/100,
      label: campaignData[i].name,
      color: colors[i]
    })
  }
}

jQuery(document).ready(function() {
  var d = new Date(jQuery("#datePicker").val());
  getData();
  jQuery("#datePicker").unbind("change").change(function() {
    var d = new Date(jQuery("#datePicker").val());
    currentDayperiod = Math.floor(d.getTime() / (1000*60*60*24));
    changeDay();
  })
})
