var CampaignManager = CampaignManager || {};

CampaignManager.campaignEditor = function(params) {

  var campaign = params.campaign;

  var initialize = function() {
    populateFields();
    jQuery("#addGroup").unbind("click").click(function() {
      createNewGroup();
    })
    jQuery("#campaignName").keyup(function() {
      var val = jQuery(this).val();
      jQuery("#campaignSLUG").val(val.replace(/ /g,"_").toLowerCase());
    })
    jQuery("#groupList").empty();
  }

  var populateFields = function() {
    jQuery("#overallBudget").val(campaign.overallBudget);
    jQuery("#campaignSponsor").val(campaign.sponsor);
    jQuery("#campaignName").val(campaign.name);
    jQuery("#campaignFrom").val(campaign.campaignFrom);
    jQuery("#campaignTo").val(campaign.campaignTo);
    // populateOutlets();
  }

  var createNewGroup = function() {
    var group = {
      incoming: [],
      outgoing: [],
      voiceText: {

      }
    };
    campaign.groups.push(group);
    var tmpl = jQuery("#groupTemplates").find(".groupOverview")[0].cloneNode(true);
    jQuery(tmpl).appendTo("#groupList");
    jQuery(tmpl).find(".addNewIncoming").click(function() {
      addNewIncoming(group, tmpl);
    })
    jQuery(tmpl).find(".addNewOutgoing").click(function() {
      addNewOutgoing(group, tmpl);
    })
  }

  var addNewIncoming = function(group, parent) {
    var tmpl = jQuery("#groupTemplates").find(".incomingRow")[0].cloneNode(true);
    jQuery(parent).find(".incomingTbody").append(tmpl);
    jQuery(tmpl).find(".removeIncoming").click(function() {
      jQuery(tmpl).remove();
    })
  }

  var addNewOutgoing = function(group, parent) {
    var tmpl = jQuery("#groupTemplates").find(".outgoingRow")[0].cloneNode(true);
    jQuery(parent).find(".outgoingTbody").append(tmpl);
    var checkOptionAndLocation = function() {
      var val1 = jQuery(tmpl).find(".optionService").val();
      var val2 = jQuery(tmpl).find(".optionLocation").val();
      if ( val1 != "0" && val2 != "0" ) {
        var num = "443-555-";
        for ( var i = 0; i < 4; i++ ) {
          num += Math.floor(Math.random()*10);
        }
        jQuery(tmpl).find(".optionNumber").text(num);
      }
    }
    jQuery(tmpl).find(".optionService").change(function() {
      checkOptionAndLocation();
    })
    jQuery(tmpl).find(".optionLocation").change(function() {
      checkOptionAndLocation();
    })
    jQuery(tmpl).find(".removeOutgoing").click(function() {
      jQuery(tmpl).remove();
    })
  }

  var createNewOutlet = function() {
    campaign.outlets.push({
      phoneNumbers: [],
      webUrls: [],
      budget: 0,
      name: "",
      type: "phone"
    });
    console.log(campaign);
    populateOutlets();
  }

  var populateOutlets = function() {
    jQuery("#outletList").empty();
    for ( var i = 0; i < campaign.outlets.length; i++ ) {
      jQuery("#outletList").append(createOutletItem(campaign.outlets[i]));
    }
  }

  var createOutletItem = function(outlet) {
    var template = jQuery("#outletTemplate").find(".outletItem")[0].cloneNode(true);
    var populatePhoneNumbers = function() {
      jQuery(".phoneNumberList",template).empty();
      console.log(outlet.phoneNumbers);
      for ( var i = 0; i < outlet.phoneNumbers.length; i++ ) {
        var div = document.createElement("div");
        jQuery(div).text(outlet.phoneNumbers[i]);
        console.log(jQuery(".phoneNumberList",template));
        jQuery(".phoneNumberList",template).append(div);
      }
    }
    populatePhoneNumbers();
    var populateWebUrls = function() {
      jQuery(".webURLsList",template).empty();
      for ( var i = 0; i < outlet.webUrls.length; i++ ) {
        var div = document.createElement("div");
        jQuery(div).text(outlet.webUrls[i]);
        jQuery(".webURLsList",template).append(div);
      }
    }
    populateWebUrls();
    jQuery(".outletName", template).unbind("change").change(function() {
      outlet.name = jQuery(this).val();
    }).val(outlet.name);
    jQuery(".outletType", template).unbind("change").change(function() {
      outlet.type = jQuery(this).val();
      checkOutletType(outlet, template);
    }).val(outlet.type);
    jQuery(".outletBudget", template).unbind("keyup").keyup(function() {
      outlet.budget = jQuery(this).val();
      updateBudgetNumbers(template);
    }).val(outlet.budget);
    jQuery(".generatePhoneNumber",template).unbind('click').click(function() {
      var numb = "";
      for ( var i = 0; i < 10; i++ ) {
          numb += Math.floor(Math.random()*9);
      }
      outlet.phoneNumbers.push(numb);
      populatePhoneNumbers();
    });
    jQuery(".generateURL",template).unbind('click').click(function() {
      var webUrl = "http://medstarhealth.bit.ly/" + Math.floor(Math.random()*10000000);
      outlet.webUrls.push(webUrl);
      populateWebUrls();
    });
    checkOutletType(outlet, template);
    return template;
  }

  var updateBudgetNumbers = function(template) {
    var overallBudget = 0;
    for ( var i = 0; i < campaign.outlets.length; i++ ) {
      try {
        overallBudget += parseInt(campaign.outlets[i].budget);
      } catch(e) {

      }
    }
    jQuery("#overallBudget").val(overallBudget);
    campaign.budget = overallBudget;
  }

  var checkOutletType = function(outlet, template) {
    if ( outlet.type == "phone" ) {
      jQuery(".generatePhoneContainer",template).show();
      jQuery(".generateWebContainer",template).hide();
    } else if ( outlet.type == "web" ) {
      jQuery(".generatePhoneContainer",template).hide();
      jQuery(".generateWebContainer",template).show();
    }
  }

  initialize();

}
